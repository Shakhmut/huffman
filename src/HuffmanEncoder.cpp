#include "HuffmanEncoder.h"
#include "HuffmanTree.h"
#include "Logging.h"
#include <algorithm>
#include <iostream>
#include <iterator>

namespace huffman {

static constexpr size_t BUFFER_SIZE = 1024;

HuffmanEncoder::HuffmanEncoder()
    : m_codeTree(std::make_unique<HuffmanTree>())
{
}

HuffmanEncoder::~HuffmanEncoder() = default;

void HuffmanEncoder::encode(std::istream &is, std::ostream &os) {
    char readBuffer[BUFFER_SIZE];
    std::streamsize readCount {0};

    while (is) {
        readCount = is.read(readBuffer, BUFFER_SIZE).gcount();
        debug() << "Read " << readCount << " bytes from input\n";
        for (int i = 0; i < readCount; ++i) {
            debug() << "Process " << char2hex(readBuffer[i]) << " symbol from input\n";
            auto codeBuffer = m_codeTree->encode(readBuffer[i]);
            debug() << "New code after processing: " << codeBuffer << '\n';

            // TODO: make output buffer and write floating-length code to it.
        }
    };
}

void HuffmanEncoder::decode(std::istream &is, std::ostream &os) {
    //TODO
}

}
