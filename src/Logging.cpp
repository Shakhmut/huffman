
#include "Logging.h"
#include <iostream>
#include <fstream>

namespace huffman {

CharPrintWrapper<BinaryPrintPolicy> char2bin(char c) {
    return CharPrintWrapper<BinaryPrintPolicy>(c);
}

CharPrintWrapper<HexPrintPolicy> char2hex(char c) {
    return CharPrintWrapper<HexPrintPolicy>(c);
}

std::ostream& debug() {
#ifndef NDEBUG
    return std::cout;
#else
    static std::ofstream dummy;
    return dummy;
#endif
}

}
