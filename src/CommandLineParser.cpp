#include "CommandLineParser.h"
#include <boost/program_options.hpp>
#include <iostream>

namespace huffman {

CommandLineParser::CommandLineParser(int argc, char* argv[]) {
    std::ios_base::sync_with_stdio(false);

    namespace bpo = boost::program_options;

    std::string ifname, ofname;
    bpo::options_description desc{"Options"};
    desc.add_options()
            ("help,h", "Help screen")
            ("in,i", bpo::value<std::string>(&ifname)->required(), "Input file name.")
            ("out,o", bpo::value<std::string>(&ofname)->required(), "Output file name.")
            ("encode,e", "Compress file or input stream.")
            ("decode,d", "Extract data from compressed file or input stream.");

    bpo::command_line_parser parser{argc, argv};
    parser.options(desc);
    bpo::parsed_options parsed_options = parser.run();

    bpo::variables_map vm;
    bpo::store(parsed_options, vm);

    if (vm.count("help")) {
        std::cout << desc << '\n';
        std::exit(0);
    }

    bpo::notify(vm);

    const char* encodeOption = "encode";
    const char* decodeOption = "decode";
    if (vm.count(encodeOption) && vm.count(decodeOption)) {
        throw std::logic_error("Conflicting encoder types.");
    } else if (!vm.count(encodeOption) && !vm.count(decodeOption)) {
        throw std::logic_error("Encoder type is not specified.");
    }

    m_encoderType = vm.count(encodeOption) ? EncoderType::ENCODE : EncoderType::DECODE;
    m_ifname = ifname;
    m_ofname = ofname;
}

}
