#pragma once

#include <iosfwd>
#include <memory>
#include <string>

namespace huffman {

class IOProvider {
public:
    IOProvider(const std::string& inputFileName, const std::string& outputFileName);
    ~IOProvider();

    std::istream& getInputStream() const;
    std::ostream& getOutputStream() const;

private:
    IOProvider(const IOProvider&) = delete;
    IOProvider& operator=(const IOProvider&) = delete;

    std::unique_ptr<std::istream> m_is;
    std::unique_ptr<std::ostream> m_os;
};

}
