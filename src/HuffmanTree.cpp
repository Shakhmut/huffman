#include "HuffmanTree.h"
#include "Logging.h"
#include <iostream>
#include <stack>

namespace huffman {


struct HuffmanTree::Node {

    Node() = default;
    Node(Node* parent)
        : parent(parent)
    {
    }
    Node(Node* parent, size_t weight, char symbol)
        : parent(parent)
        , weight(weight)
        , symbol(symbol)
    {
    }

    bool isRootNode() const {
        return parent == nullptr;
    }

    bool isLeafNode() const {
        return !leftChild && !rightChild;
    }

    bool isRightChild() const {
        return parent && parent->rightChild.get() == this;
    }

    Node* parent {nullptr};
    std::unique_ptr<Node> leftChild;
    std::unique_ptr<Node> rightChild;
    size_t weight {0}; // frequency
    char symbol {0};
};

HuffmanTree::HuffmanTree()
    : m_root(std::make_unique<Node>())
    , m_zeroNode(m_root.get())
{
}

HuffmanTree::~HuffmanTree() = default;

BitBuffer HuffmanTree::encode(const char symbol) {
    BitBuffer buf;
    if (m_leaves.count(symbol)) {
        buf = getSymbolCode(m_leaves[symbol]);
        m_leaves[symbol]->weight++;
    } else {
        // If new symbol is added, it's code will consist of zero-leaf code + symbol itself. For unambigious decoding.
        buf = getSymbolCode(m_zeroNode);
        buf.append(symbol);
        addNewLeaf(symbol);
    }
    //update parent weights and tree structure
    updateTree(m_leaves[symbol]);

#ifndef NDEBUG
    dumpTree();
#endif

    return buf;
}

void HuffmanTree::addNewLeaf(const char symbol) {
    m_zeroNode->leftChild = std::make_unique<Node>(m_zeroNode);
    m_zeroNode->rightChild = std::make_unique<Node>(m_zeroNode, 1, symbol);
    // right child is a new leaf
    m_leaves[symbol] = m_zeroNode->rightChild.get();
    m_zeroNode->weight = 1;
    // and left child is a new zero node
    m_zeroNode = m_zeroNode->leftChild.get();
}

void HuffmanTree::updateTree(Node* node) {
    while ((node = node->parent) != nullptr) {
        node->weight++;
        //TODO: Implement "heart" of algorithm. How to efficiently reorder nodes to maintain "sibling" rule?
    }
}

BitBuffer HuffmanTree::getSymbolCode(const Node* node) const {
    BitBuffer buffer;
    // TODO: Just for simplicity, find a "faster" solution how to deal with reversed codes.
    std::stack<bool> directOrderBits;
    while (node->parent) {
        directOrderBits.push(node->isRightChild());
        node = node->parent;
    }
    buffer.bitsCount = directOrderBits.size();
    size_t charBitCounter {CHAR_BIT};
    char c{0};
    while (!directOrderBits.empty()) {
        if (directOrderBits.top()) {
            c |= (1 << (charBitCounter - 1));
        }
        // byte completed
        if (--charBitCounter == 0) {
            charBitCounter = CHAR_BIT;
            buffer.emplace_back(c);
            c = 0;
        }
        directOrderBits.pop();
    }

    // check for the last partially filled byte
    if (charBitCounter != CHAR_BIT) {
        buffer.emplace_back(c);
    }

    return buffer;
}

void HuffmanTree::dfs(std::function<void (const Node *)> visitor) const {
    std::stack<Node*> nodes;
    auto currentNode = m_root.get();

    while (currentNode || !nodes.empty()) {
        while (currentNode) {
            nodes.push(currentNode);
            currentNode = currentNode->leftChild.get();
        }
        visitor(nodes.top());
        currentNode = nodes.top()->rightChild.get();
        nodes.pop();
    }
}

void HuffmanTree::dumpTree() const {
    dfs([zeroNode = m_zeroNode](const Node* node) {
        debug() << "[";
        if (node->isLeafNode() && node != zeroNode) {
            debug() << char2hex(node->symbol) << ", ";
        }
        debug() << node->weight << "] ";
    });
    debug() << '\n';
}

}
