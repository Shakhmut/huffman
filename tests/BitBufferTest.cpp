#include <boost/test/unit_test.hpp>
#include "BitBuffer.h"

BOOST_AUTO_TEST_SUITE(BitBufferTest)

// TODO: add description to each test case

BOOST_AUTO_TEST_CASE(append_simple)
{
    huffman::BitBuffer buffer;
    buffer.append(static_cast<char>(0xFF));
    BOOST_CHECK_EQUAL(buffer.size(), 1);
    BOOST_CHECK_EQUAL(buffer.bitsCount, 8);
    BOOST_CHECK_EQUAL(buffer.front(), static_cast<char>(0xFF));
}

BOOST_AUTO_TEST_CASE(append_complex1)
{
    huffman::BitBuffer buffer;
    buffer.push_back(static_cast<char>(0xF0));
    buffer.bitsCount = 4;
    buffer.append(static_cast<char>(0xDE));
    BOOST_CHECK_EQUAL(buffer.size(), 2);
    BOOST_CHECK_EQUAL(buffer.bitsCount, 12);
    BOOST_CHECK_EQUAL(buffer[0], static_cast<char>(0xFD));
    BOOST_CHECK_EQUAL(buffer[1], static_cast<char>(0xE0));
}

BOOST_AUTO_TEST_CASE(append_complex2)
{
    huffman::BitBuffer buffer;
    buffer.push_back(static_cast<char>(0x80));
    buffer.bitsCount = 1;
    buffer.append(static_cast<char>(0xFF));
    BOOST_CHECK_EQUAL(buffer.size(), 2);
    BOOST_CHECK_EQUAL(buffer.bitsCount, 9);
    BOOST_CHECK_EQUAL(buffer[0], static_cast<char>(0xFF));
    BOOST_CHECK_EQUAL(buffer[1], static_cast<char>(0x80));
}

BOOST_AUTO_TEST_CASE(append_complex3)
{
    huffman::BitBuffer buffer;
    buffer.push_back(static_cast<char>(0x0E));
    buffer.bitsCount = 7;
    buffer.append(static_cast<char>(0xFF));
    BOOST_CHECK_EQUAL(buffer.size(), 2);
    BOOST_CHECK_EQUAL(buffer.bitsCount, 15);
    BOOST_CHECK_EQUAL(buffer[0], static_cast<char>(0x0F));
    BOOST_CHECK_EQUAL(buffer[1], static_cast<char>(0xFE));
}

BOOST_AUTO_TEST_SUITE_END()
