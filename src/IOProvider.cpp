#include "IOProvider.h"
#include <iostream>
#include <fstream>

namespace huffman {

IOProvider::IOProvider(const std::string& inputFileName, const std::string& outputFileName) {
    m_is = std::make_unique<std::ifstream>(inputFileName, std::ios_base::in | std::ios_base::binary);
    if (!(*m_is)) {
        throw std::logic_error(std::string("Can not open an input file ") + inputFileName);
    }
    m_os = std::make_unique<std::ofstream>(outputFileName, std::ios_base::out | std::ios_base::binary);
    if (!(*m_os)) {
        throw std::logic_error(std::string("Can not open an output file ") + outputFileName);
    }
}

IOProvider::~IOProvider() = default;

std::istream& IOProvider::getInputStream() const {
    return *m_is;
}

std::ostream& IOProvider::getOutputStream() const {
    return *m_os;
}

}
