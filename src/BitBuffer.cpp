
#include "BitBuffer.h"
#include "Logging.h"
#include <climits>
#include <ostream>
#include <bitset>

namespace huffman {

void BitBuffer::append(char symbol) {
    size_t bitsInLastByte = bitsCount % CHAR_BIT;
    // first occupy "free" bits in the last byte
    if (bitsInLastByte > 0) {
        // be careful with right shift of signed-type value.
        back() |= (static_cast<unsigned char>(symbol) >> bitsCount);
        symbol <<= (CHAR_BIT - bitsCount);
    }
    emplace_back(symbol);
    bitsCount += CHAR_BIT;
}

std::ostream& operator<<(std::ostream& os, const BitBuffer& buffer) {
    for (const auto& c : buffer) {
        os << char2bin(c);
    }
    return os;
}

}
