#pragma once

#include <string>

namespace huffman {

enum class EncoderType {
    ENCODE,
    DECODE
};

class CommandLineParser {
public:
    CommandLineParser(int argc, char* argv[]);

    const std::string& getInputFileName() const {
        return m_ifname;
    }

    const std::string& getOutputFileName() const {
        return m_ofname;
    }

    EncoderType getEncoderType() const {
        return m_encoderType;
    }

private:
    CommandLineParser(const CommandLineParser&) = delete;
    CommandLineParser& operator=(const CommandLineParser&) = delete;

    std::string m_ifname;
    std::string m_ofname;
    EncoderType m_encoderType;
};

}
