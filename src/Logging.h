#pragma once

#include <iosfwd>
#include <iomanip>
#include <bitset>
#include <climits>

namespace huffman {

struct BinaryPrintPolicy {
    static std::ostream& print(std::ostream& os, char c) {
        return os << std::bitset<CHAR_BIT>(static_cast<unsigned char>(c));
    }
};

struct HexPrintPolicy {
    static std::ostream& print(std::ostream& os, char c) {
        return os << std::hex << std::uppercase << static_cast<short>(c);
    }
};

template<typename PrintPolicy = HexPrintPolicy>
struct CharPrintWrapper {
    CharPrintWrapper(char c)
        : c(c)
    {
    }

    std::ostream& print(std::ostream& os) const {
        return PrintPolicy::print(os, c);
    }

    char c;
};

template<typename PrintPolicy>
std::ostream& operator<<(std::ostream& os, const CharPrintWrapper<PrintPolicy>& wrapper) {
    return wrapper.print(os);
}

CharPrintWrapper<BinaryPrintPolicy> char2bin(char c);
CharPrintWrapper<HexPrintPolicy> char2hex(char c);

std::ostream& debug();

}
