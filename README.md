# README #

Simple adaptive Huffman encoder.

# build #
cd <build_dir>

cmake -r <huffman_repo_dir>

make


# run #
./huffman -h
