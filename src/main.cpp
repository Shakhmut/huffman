#include "CommandLineParser.h"
#include "IOProvider.h"
#include "HuffmanEncoder.h"
#include <iostream>

int main(int argc, char* argv[]) {
    std::ios_base::sync_with_stdio(false);

    try {
        huffman::CommandLineParser cmdlParser(argc, argv);
        huffman::IOProvider ioProvider(cmdlParser.getInputFileName(), cmdlParser.getOutputFileName());
        huffman::HuffmanEncoder encoder;

        // factory and "one" interface + aggregate abstract IOProvider? maybe later :)
        if (cmdlParser.getEncoderType() == huffman::EncoderType::ENCODE) {
            encoder.encode(ioProvider.getInputStream(), ioProvider.getOutputStream());
        } else {
            encoder.decode(ioProvider.getInputStream(), ioProvider.getOutputStream());
        }

    } catch (const std::exception &ex) {
        std::cerr << ex.what() << '\n';
    } catch (...) {
        std::cerr << "Unknown exception occurred\n";
    }

    return 0;
}
