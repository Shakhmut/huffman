#include <boost/test/unit_test.hpp>
#include "HuffmanTree.h"

BOOST_AUTO_TEST_SUITE(HuffmanTreeTest)

BOOST_AUTO_TEST_CASE(encode)
{
    huffman::HuffmanTree codeTree;
    huffman::BitBuffer buffer;

    buffer = codeTree.encode(static_cast<char>(0xFF));
    BOOST_CHECK_EQUAL(buffer.size(), 1);
    BOOST_CHECK_EQUAL(buffer.bitsCount, 8);
    BOOST_CHECK_EQUAL(buffer.front(), static_cast<char>(0xFF));

    buffer = codeTree.encode(static_cast<char>(0xFF));
    BOOST_CHECK_EQUAL(buffer.size(), 1);
    BOOST_CHECK_EQUAL(buffer.bitsCount, 1);
    BOOST_CHECK_EQUAL(buffer.front(), static_cast<char>(0x80));

//    buffer = codeTree.encode(static_cast<char>(0x7F));
//    BOOST_CHECK_EQUAL(buffer.size(), 2);
//    BOOST_CHECK_EQUAL(buffer.bitsCount, 9);
//    BOOST_CHECK_EQUAL(buffer[0], static_cast<char>(0xBF));
//    BOOST_CHECK_EQUAL(buffer[1], static_cast<char>(0x80));
}

BOOST_AUTO_TEST_SUITE_END()
