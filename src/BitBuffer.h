#pragma once

#include <vector>
#include <iosfwd>

namespace huffman {

// TODO: use private inharitance or composition
struct BitBuffer : public std::vector<char> {
    void append(char symbol);

    size_t bitsCount {0};
};

std::ostream& operator<<(std::ostream& os, const BitBuffer& buffer);

}
