#pragma once

#include "BitBuffer.h"
#include <memory>
#include <functional>
#include <unordered_map>

namespace huffman {

class HuffmanTree {

public:
    HuffmanTree();
    ~HuffmanTree();

    BitBuffer encode(const char symbol);

private:
    struct Node;

    void addNewLeaf(const char symbol);
    void updateTree(Node* node);
    BitBuffer getSymbolCode(const Node* node) const;

    void dfs(std::function<void(const Node*)> visitor) const;
    void dumpTree() const;

    std::unique_ptr<Node> m_root;
    Node* m_zeroNode {nullptr};
    std::unordered_map<char, Node*> m_leaves;
};

}
