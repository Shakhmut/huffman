#pragma once

#include <functional>
#include <iosfwd>
#include <memory>

namespace huffman {

class HuffmanTree;
class HuffmanEncoder {

public:
    HuffmanEncoder();
    ~HuffmanEncoder();

    void encode(std::istream& is, std::ostream& os);
    void decode(std::istream& is, std::ostream& os);

private:
    std::unique_ptr<HuffmanTree> m_codeTree;
};

}
